package com.econnectpeople.distributor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.econnectpeople.distributor.Model.Item;
import com.econnectpeople.distributor.R;

import java.util.ArrayList;

public class StateListAdapter extends RecyclerView.Adapter<StateListAdapter.ViewHolder> implements Filterable {

    private ArrayList<Item> mArrayList;
    private ArrayList<Item> mFilteredList;
    Context ctx;

    public StateListAdapter(ArrayList<Item> arrayList, Context ctx) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
        this.ctx=ctx;
    }

    @Override
    public StateListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_state_list_adapter, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StateListAdapter.ViewHolder viewHolder, int i) {

        viewHolder.txt_operator_name.setText(mFilteredList.get(i).getOperator_name());
        viewHolder.txt_id.setText(mFilteredList.get(i).getCommission());

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Item> filteredList = new ArrayList<>();
                    for (Item androidVersion : mArrayList) {

                        if (androidVersion.getOperator_name().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_operator_name,txt_id;

        public ViewHolder(View view) {
            super(view);

            txt_operator_name = (TextView)view.findViewById(R.id.txt_operator_name);
            txt_id = (TextView)view.findViewById(R.id.txt_id);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String operator_name=txt_operator_name.getText().toString();
                    String id=txt_id.getText().toString();
                    Intent intent=new Intent();
                    intent.putExtra("operator_circle_name",operator_name);
                    intent.putExtra("operator_circle_id",id);
                    ((Activity) ctx).setResult(3,intent);
                    ((Activity) ctx).finish();//finishing activity

                }
            });

        }
    }
}
