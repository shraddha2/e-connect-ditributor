package com.econnectpeople.distributor.TransactionHistory;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.econnectpeople.distributor.EconnectPeopleDistributor;
import com.econnectpeople.distributor.Model.Item;
import com.econnectpeople.distributor.R;

import java.util.List;

public class AepsAdapter extends RecyclerView.Adapter<AepsAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTransactionId,txtAmount,txtRetailerCommission,txtUpdatedTime,txtBankTransactionId,txtTransactionType,txtRemark,txtTime,txtStatus;
        ImageView img_arrow;
        ConstraintLayout linear_more_data;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtTransactionId = (TextView) view.findViewById(R.id.txtTransactionId);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtRetailerCommission = (TextView) view.findViewById(R.id.txtRetailerCommission);
            txtUpdatedTime = (TextView) view.findViewById(R.id.txtUpdatedTime);
            txtBankTransactionId = (TextView) view.findViewById(R.id.txtBankTransactionId);
            txtTransactionType = (TextView) view.findViewById(R.id.txtTransactionType);
            txtRemark = (TextView) view.findViewById(R.id.txtRemark);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            img_arrow = (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data = (ConstraintLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility = linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE) {
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    } else {

                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public AepsAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.aeps_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txtTransactionId.setText(movie.getLabel1() + "");
        holder.txtAmount.setText(movie.getLabel2() + "");
        holder.txtRetailerCommission.setText("Distributor Commission: " + movie.getLabel3());
        holder.txtUpdatedTime.setText("Updated Time: " + movie.getLabel4() + "");
        holder.txtBankTransactionId.setText("Bank Trans Id: "+movie.getLabel5() + "");
        holder.txtTransactionType.setText("Transaction Type: "+movie.getLabel6() + "");
        holder.txtRemark.setText("Remark: " + movie.getLabel7() + "");
        holder.txtTime.setText(movie.getLabel8() + "");
        holder.txtStatus.setText(movie.getLabel9() + "");

        if (movie.getLabel9().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_fail));
        } else if (movie.getLabel9().equalsIgnoreCase("Pending")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_initiated));
        } else {
            holder.txtAmount.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(EconnectPeopleDistributor.getInstance(), R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
