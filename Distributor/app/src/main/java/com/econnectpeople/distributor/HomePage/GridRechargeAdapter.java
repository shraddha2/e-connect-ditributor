package com.econnectpeople.distributor.HomePage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.econnectpeople.distributor.Model.Item;
import com.econnectpeople.distributor.R;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class GridRechargeAdapter extends BaseAdapter {
    private final Context mContext;
    List<Item> horizontalList = Collections.emptyList();

    public GridRechargeAdapter(List<Item> horizontalList, Context context) {
        this.horizontalList = horizontalList;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return horizontalList.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Item book = horizontalList.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.vertical_menu, null);
        }

        final ImageView imageView = (ImageView)convertView.findViewById(R.id.imageview);
        final TextView nameTextView = (TextView)convertView.findViewById(R.id.txtview);
        final TextView txt_id = (TextView)convertView.findViewById(R.id.txt_id);

        Picasso.with(mContext).load(horizontalList.get(position).imageId).fit().into(imageView);
        nameTextView.setText(horizontalList.get(position).txt);
        return convertView;
    }

    // Your "view holder" that holds references to each subview
    private class ViewHolder {
        private final TextView nameTextView;
        private final TextView txt_id;
        private final ImageView imageViewFavorite;
        //Typeface font ;
        public ViewHolder(TextView nameTextView, TextView txt_id, ImageView imageViewFavorite) {

            this.nameTextView = nameTextView;
            this.txt_id = txt_id;
            this.imageViewFavorite = imageViewFavorite;
        }
    }
}