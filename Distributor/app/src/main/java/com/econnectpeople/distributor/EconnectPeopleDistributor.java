package com.econnectpeople.distributor;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.econnectpeople.distributor.Receiver.ConnectivityReceiver;

public class EconnectPeopleDistributor extends Application {

    private static EconnectPeopleDistributor mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized EconnectPeopleDistributor getInstance() {
        return mInstance;


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}

